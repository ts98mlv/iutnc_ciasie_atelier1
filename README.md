# iutnc_ciasie_atelier1

## groupe
Thomas SERRES <br>
Théo LEGRAND <br>
Théo HELF <br>
Yann Le LAN <br>

## URLs
 - staff : https://webetu.iutnc.univ-lorraine.fr/www/serres4u/iutnc_ciasie_atelier1/medianet-staff/ <br>
 - usagers : https://webetu.iutnc.univ-lorraine.fr/www/serres4u/iutnc_ciasie_atelier1/medianet-usagers/ <br>
 - dépôt git : https://gitlab.com/ts98mlv/iutnc_ciasie_atelier1 <br>
 - tableau de bord : https://trello.com/b/t3nkjW8Q/atelier1

## Données de test:
Sur Webetu vous disposez d'un compte de test dont l'email est <code>test@test.fr</code> et le mot de passe <code>Clm2pTest</code>.  <br>
Il y a aussi 3 documents (un livre, un cd et un dvd). Voici leurs caractéristiques : <br>
 - livre : <br>
        * id : 1 <br>
        * titre : Astérix - tome 38- La Fille de Vercingétorix <br>
  - dvd : <br>      
        * id : 2 <br>
        * titre : 50 nuances plus claires <br>
  - cd : <br>
        * id : 3 <br>
        * titre : Africa best of  <br>      
 

## Etat d'un document
La variable 'etat' définit l'état d'un document : <br>
 - 0 : disponible <br>
 - 1 : emprunté <br>
 - 2 : indisponible

## Type d'un document dans la BDD
La variable 'type' définit le type d'un document : <br>
 - 0 : Livre <br>
 - 1 : DVD <br>
 - 2 : CD <br>
 
## Demandes d'adhésion
La variable "en_attente" définit le statut d'une demande d'adhésion : <br>
  - 0 : accepté <br>
  - 1 : en attente <br>
  - 2 : refusé <br>
  
## statuts du mot de passe
 La variable "m2p_status" définit le statut du mot de passe : <br> 
    - 0 : ok <br>
    - 1 : provisoire <br>
    
        
## problèmes connus
- problème de temps en temps sur webetu pour emprunter/rendre un document (mais marche parfaitement en local)
- sur webetu, envoie des mails uniquements à des adresses mail de l'université de Lorraine.        


## installation en local
- cloner le dépôt git <br>
- dans chaque partie de Médianet (medianet-staff et medianet-usagers), faire composer install <br>
- créer une base de données "medianet" et y importer les scripts bdd.sql, et les différents patch SQL <br>
- Pour chaque partie, dans <code>src/config</code>, copier coller le document exempleconfig.inc.php et le renommer en config.inc.php <br>
- y entrer les informations nécessaires pour se connecter à la BDD 
- modifier le .htaccess si besoin en fonction de l'endroit où vous stockez Médianet