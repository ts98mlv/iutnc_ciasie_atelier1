CREATE DATABASE IF NOT EXISTS `medianet`;
use `medianet`;

CREATE TABLE IF NOT EXISTS DOCUMENT(
  document_id int(50) AUTO_INCREMENT,
  etat varchar(50) not null,
  genre varchar(500),
  titre varchar(500) not null,
  description varchar(500),
  mots_cles varchar(500),
  type int(5) not null,
  duree int(25),
  acteurs varchar(100),
  nbPages int(50),
  editeur varchar(50),
  illustration text,
  deleted_at datetime,
  PRIMARY KEY (document_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS USAGER(
  usager_id int(50) AUTO_INCREMENT,
  nom varchar(50) not null,
  prenom varchar(50),
  adresse text not null,
  mail varchar(60) not null unique,
  tel varchar(45) not null ,
  date_adhesion date,
  m2p varchar(1024) not null,
  deleted_at datetime,
  PRIMARY KEY (usager_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS EMPRUNT(
  emprunt_id int(50) AUTO_INCREMENT,
  usager_id int(50),
  document_id int(50),
  date_emprunt date,
  date_retour_prevue date,
  date_retour_effective date,
  deleted_at datetime,
  FOREIGN KEY (usager_id) references USAGER(usager_id),
  FOREIGN KEY (document_id) references DOCUMENT(document_id) ,
  PRIMARY KEY (emprunt_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
