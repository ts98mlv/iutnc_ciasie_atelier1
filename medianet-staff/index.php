<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require __DIR__ . '/src/config/config.inc.php';
require __DIR__ . '/vendor/autoload.php';

// Create container
$container = array();


///////////
// TWIG //
//////////

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('src/views', ["debug" => true]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    $view->addExtension(new \Twig_Extension_Debug());

    return $view;
};


///////////////
// ELOQUENT //
//////////////
$container['settings'] = $config;
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

// Init Slim
$app = new \Slim\App($container);



//session_start
session_start();


//////////////
// ROUTAGE //
/////////////

//Page Accueil
$app->get('/','\\medianet_staff\\controllers\\StaffController:afficherAccueil');

// créer un document
$app->get("/createDoc", "\\medianet_staff\\controllers\\StaffController:formDoc")->setName('createDoc');
$app->post('/createDoc', "\\medianet_staff\\controllers\\StaffController:createDoc");

//Page d'accueil, récapitulant tous les emprunts en cours.
$app->get('/dashboard','\\medianet_staff\\controllers\\StaffController:afficherDashboard')->setName("dashBoard");

//Page pour emprunter des livres
$app->get('/emprunts','\\medianet_staff\\controllers\\StaffController:emprunts');

//Page pour le retour des livres
$app->get('/retours','\\medianet_staff\\controllers\\StaffController:retours');

//Page pour la recherche d'un document selon un critère (ID ou partie du titre)
$app->get('/chercherDocument/{critere}','\\medianet_staff\\controllers\\StaffController:rechercherDocument');

//Page pour rechercher un utilisateur selon un critère (ID ou adresse email. (complète))
$app->get('/chercherUtilisateur/{critere}','\\medianet_staff\\controllers\\StaffController:rechercherUtilisateur');

//Page gérant le rendu en base de données.
$app->post('/rendreDocuments','\\medianet_staff\\controllers\\StaffController:rendreDocuments');

//Page de récap des emprunts
$app->get('/recapEmprunts/{id}','\\medianet_staff\\controllers\\StaffController:recapEmprunts');

//Page gérant les emprunts en base de données
$app->post('/emprunterDocuments','\\medianet_staff\\controllers\\StaffController:emprunterDocuments');

$app->get("/ajouterUsager", "\\medianet_staff\\controllers\\StaffController:afficherAjoutUsager")->setName("ajoutUsager");

$app->post("/ajouterUsager", "\\medianet_staff\\controllers\\StaffController:ajoutUsager");

$app->get("/listeDocs", "\\medianet_staff\\controllers\\StaffController:listeDoc")->setName("listeDoc");

$app->get('/modifDoc{id}', "\\medianet_staff\\controllers\\StaffController:formDocModif")->setName('modifDoc');
$app->post('/modifDoc{id}', "\\medianet_staff\\controllers\\StaffController:modifDoc");

$app->get('/supprimerDoc{id}', "\\medianet_staff\\controllers\\StaffController:supprimerDoc")->setName('supprimerDoc');


$app->get("/voirUsagers", "\\medianet_staff\\controllers\\StaffController:voirUsagers")->setName("voirUsagers");

$app->get("/supprimerUsager{id}", "\\medianet_staff\\controllers\\StaffController:supprimerUsager")->setName("supprimerUser");

$app->get("/modifierUsager/{id}", "\\medianet_staff\\controllers\\StaffController:afficherModifierUsager");
$app->post("/modifierUsager", "\\medianet_staff\\controllers\\StaffController:modifierUsager");

//gestion des demandes d'adhesion
$app->get("/demandesAdhesion", "\\medianet_staff\\controllers\\StaffController:afficherGestionAdhesion")->setName("gererDemandesAdhesion");
$app->get("/accepterAdhesion/{id}", "\\medianet_staff\\controllers\\StaffController:accepterAdhesion");
$app->get("/refuserAdhesion/{id}", "\\medianet_staff\\controllers\\StaffController:refuserAdhesion");

//envoie des rappels
$app->get("/rappel/{emprunt_id}", "\\medianet_staff\\controllers\\StaffController:envoieMailRappel");

//réinitialisation de mot de passe
$app->get("/reinitMdp/{id}", "medianet_staff\\controllers\\StaffController:reinitialiserMdp");

/////////////
// RUN     //
/////////////
$app->run();
