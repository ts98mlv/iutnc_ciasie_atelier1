let tableauID = [];
let utilisateurID = null;


$(document).ready(() => {

    $("#bodyTableauUtilisateur").on('click', '.supprUsager', function()   {
        utilisateurID = null;
        $($(this).parent().parent()).remove()
    });

    $("#bodyTableauDocs").on('click', '.supprDoc', function()   {
        console.log(tableauID);
        for (let i = 0; i < tableauID.length; i++) {
            if ( tableauID[i] == $(this)[0].id ){
                tableauID.splice(i,1);
            }
        }
        console.log(tableauID);
        $($(this).parent().parent()).remove()
    });

    $('#ajouterEmprunt').click(() => {
        let val = $('#docAChercher').val();
        console.log(val);
        if (val !== "") {
            $('#docAChercher').val("");
            fetch("chercherDocument/" + val)
                .then(function (rep) {
                    rep.json().then(function (rep) {
                        if (!(rep.document === null)) {
                            ajouterTableauDoc(rep.document);
                        } else {
                            $('#ajouterEmprunt').notify("Le document est introuvable !", "error");
                        }
                    })
                });
        }
    });

    $('#chercherUser').click(() => {
        let user = $('#userAChercher').val();
        if (user !== "") {
            $('#userAChercher').val("");
            fetch("chercherUtilisateur/" + user)
                .then((rep) => {
                    rep.json().then((rep) => {
                        if (!(rep.usager === null) && utilisateurID === null) {
                            if (utilisateurID !== rep.usager.usager_id) {
                                utilisateurID = rep.usager.usager_id
                                ajouterTableauUsager(rep.usager);
                            }
                        } else if(utilisateurID !== null){
                            $('#chercherUser').notify("Un seul utilisateur à la fois !", "error");
                        } else {
                            $('#chercherUser').notify("Utilisateur non trouvé !", "error");
                        }
                    })
                })
        }
    });

    $('#emprunterDocs').click(() => {
        if (tableauID.length === 0){
            $('#rendreDocs').notify("Aucun document à emprunter !","warn");
        } else {
            fetch("emprunterDocuments", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify([tableauID,utilisateurID])
            }).then((rep) => {
                if (rep.ok){
                    window.location.replace($('#retourHome').val()+"/"+utilisateurID);
                }
            })
        }
    });


});

function ajouterTableauUsager(usager) {
    $('#bodyTableauUtilisateur')[0].innerHTML += "<tr>"
            + "<td>"+ usager.usager_id + "</td>"
            + "<td>" + usager.nom + "</td>"
            + "<td>" + usager.prenom + "</td>"
            + "<td>" + usager.mail + "</td>"
            + "<td><a class='supprUsager'><span class='far fa-trash-alt icon-tab' style='color: #ff4f59'></span></a></td>"
        + "</tr>";
}

function ajouterTableauDoc(document){
    if (tableauID.indexOf(document.document_id) === -1 && document.etat === "0") {
        tableauID.push(document.document_id);
        switch(document.type){
            case 0:
                document.type = "Livre";
                break;
            case 1:
                document.type = "DVD";
                break;
            case 2:
                document.type = "CD";
                break;
            default:
                break;
        }
        $('#bodyTableauDocs')[0].innerHTML += "<tr>" +
            "<td>" + document.document_id + "</td>" +
            "<td>" + document.titre + "</td>"+
            "<td>" + document.type + "</td>" +
            "<td>" + document.genre + "</td>" +
            "<td>" + document.auteur + "</td>" +
            "<td><a class='supprDoc' id='" + document.document_id + "'><span class='far fa-trash-alt icon-tab' style='color: #ff4f59'></a></td>"
            + "</tr>";
    } else if (document.etat === "1"){
        $('#ajouterEmprunt').notify("Le document est déjà emprunté !","error");
    } else {
        $('#ajouterEmprunt').notify("Le document ne peut pas être emprunté !","warn");
    }
};