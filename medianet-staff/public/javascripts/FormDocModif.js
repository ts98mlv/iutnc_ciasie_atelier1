$(document).ready(function () {
    // verif si un element est sélectionné dans les listes déroulantes
    $("select[required]").css({display: "block", height: 0, padding: 0, width: 0, position: 'absolute'});
    // pour afficher le select
    $('select').formSelect();
    // Trouve l'option selectionné
    let $option = $(this).find('option:selected');
    
    //Ajoute dans une variable la valeur 
    let value = $option.val();
    
    // console.log(value);
    if (value == 0) {
        $("#jsnbPages").show();
        $("#jsediteur").show();
        $("#jsacteurs").hide();
        $("#jsduree").hide();
    }
    else if (value == 1) {
        $("#jsnbPages").hide();
        $("#jsediteur").hide();
        $("#jsacteurs").show();
        $("#jsduree").show();
    }
    else if (value == 2) {
        $("#jsnbPages").hide();
        $("#jsediteur").hide();
        $("#jsacteurs").hide();
        $("#jsduree").show();
    }
})