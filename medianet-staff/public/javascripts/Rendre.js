let tableauID = [];

function insertValues() {
    let chaine = "";
    tableauID.forEach((valeur,index)=>{
        chaine += valeur;
        console.log("Index : " + index);
        console.log("Valeur : " + valeur);
        if (index < tableauID.length - 1 && tableauID.length >= 2){
            chaine += ",";
        }
    })
    return chaine;
}

$(document).ready(() => {

    $("#bodyTableau").on('click', '.suppr', function()   {
        console.log(tableauID);
        for (let i = 0; i < tableauID.length; i++) {
            if ( tableauID[i] == $(this)[0].id ){
                tableauID.splice(i,1);
            }
        }
        console.log(tableauID);
        $($(this).parent().parent()).remove()
    });

    $('#ajouterRendre').click(() => {
        let val = $('#docAChercher').val();
        $('#docAChercher').val("");
        fetch("chercherDocument/" + val)
            .then(function (rep) {
               rep.json().then(function (rep) {
                   if (!(rep.document === null)) {
                       ajouterTableau(rep.document);
                       $('#docsARendre').val(insertValues())
                   } else {
                       $('#ajouterRendre').notify("Le document est introuvable !","error");
                   }
                })
            });

    });

    $('#rendreDocs').click((event) => {
        if (tableauID.length === 0){
            event.preventDefault()
            $('#rendreDocs').notify("Aucun document à rendre !","warn");
        }
    });


});



function ajouterTableau(document){
    if (tableauID.indexOf(document.document_id) === -1 && document.etat === "1") {
        tableauID.push(document.document_id);
        switch(document.type){
            case 0:
                document.type = "Livre";
                break;
            case 1:
                document.type = "DVD";
                break;
            case 2:
                document.type = "CD";
                break;
            default:
                break;
        }
        $('#bodyTableau')[0].innerHTML +=
            "<tr>" +
                "<td>" + document.document_id + "</td>" +
                "<td>" + document.titre + "</td>"+
                "<td>" + document.type + "</td>" +
                "<td>" + document.genre + "</td>" +
                "<td>" + document.auteur + "</td>" +
                "<td><a class=\"suppr\" id=\"" + document.document_id + "\"><span class='far fa-trash-alt icon-tab' style='color: #ff4f59'></a></td>"
            + "</tr>";
    } else if (document.etat === "0"){
        $('#ajouterRendre').notify("Le document n'est pas emprunté !","error");
    } else {
        $('#ajouterRendre').notify("Le document n'est pas disponible à l'emprunt !","warn");
    }
};