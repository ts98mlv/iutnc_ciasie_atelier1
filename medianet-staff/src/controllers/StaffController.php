<?php

namespace medianet_staff\controllers;

use medianet_staff\models\Document;
use medianet_staff\models\Usager;
use medianet_staff\models\Emprunt;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class StaffController extends BaseController{

    /**
     * méthode qui permet d'afficher le formulaire pour créer un perso'
     * @param $request
     * @param $response
     * @return mixed
     */
    public function formDoc($request,$response) {
       return $this->render($response, 'FormDoc.html.twig');
    }//End of function formDoc

    /**
     * Fonction permettant de créer le document en BDD
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     * @throws \Exception
     */
    public  function createDoc($request, $response, $args)
    {

        //chemin stockage images
        $cheminImg = $GLOBALS['PUBLIC'] . "images/";

        // récupération des données
        $genre = (!empty($_POST['genre'])) ? $_POST["genre"] : "";
        $titre = (!empty($_POST['titre'])) ? $_POST["titre"] : "";
        $auteur = (!empty($_POST['auteur'])) ? $_POST["auteur"] : "";
        $description = (!empty($_POST['description'])) ? $_POST["description"] : "";
        $mots_cles = (!empty($_POST['mc'])) ? $_POST["mc"] : "";
        $type = (!empty($_POST['type'])) ? $_POST["type"] : 0;
        $duree = (!empty($_POST['duree'])) ? $_POST["duree"] : 0;
        $acteurs = (!empty($_POST['acteurs'])) ? $_POST['acteurs'] : "";
        $nbPages = (!empty($_POST['nbPages'])) ? $_POST['nbPages'] : 0;
        $editeur = (!empty($_POST['editeur'])) ? $_POST['editeur'] : "";
        //traitement des données
        $genre = filter_var($genre, FILTER_SANITIZE_STRING);
        $titre = filter_var($titre, FILTER_SANITIZE_STRING);
        $auteur = filter_var($auteur, FILTER_SANITIZE_STRING);
        $description = filter_var($description, FILTER_SANITIZE_STRING);
        $mots_cles = filter_var($mots_cles, FILTER_SANITIZE_STRING);
        $type = filter_var($type, FILTER_SANITIZE_STRING);
        $duree = filter_var($duree, FILTER_SANITIZE_NUMBER_INT);
        $acteurs = filter_var($acteurs, FILTER_SANITIZE_STRING);
        $nbPages = filter_var($nbPages, FILTER_SANITIZE_NUMBER_INT);
        $editeur = filter_var($editeur, FILTER_SANITIZE_STRING);

        $doc = new Document();
        $doc->etat = $_POST['etat'];
        $doc->genre = $genre;
        $doc->titre = $titre;
        $doc->auteur = $auteur;
        $doc->description = $description;
        $doc->mots_cles = $mots_cles;
        $doc->type = $type;
        $doc->duree = $duree;
        $doc->acteurs = $acteurs;
        $doc->nbPages = $nbPages;
        $doc->editeur = $editeur;
        if ($_POST["illustration"] == ""){
            $doc->illustration = "https://www.vermeer.com.au/wp-content/uploads/2016/12/attachment-no-image-available.png";
          }
          else {
            $doc->illustration = $_POST["illustration"];
          }

        $doc->save();
        $doc = Document::all();
        return $this->redirect($response, 'listeDoc');
    }//End of function createDoc

    /**
     * Méthode qui permet d'afficher la page de Dashboard du staff.
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherDashboard($request,$response){

        $emprunts = DB::table('DOCUMENT')
            ->join('EMPRUNT','DOCUMENT.document_id','=','EMPRUNT.document_id')
            ->join('USAGER','USAGER.usager_id','=','EMPRUNT.usager_id')
            ->where('DOCUMENT.etat','=',1)
            ->whereNull('date_retour_effective')->get();
        return $this->render($response, 'Dashboard.html.twig',['emprunts' => $emprunts]);
    }//End of function afficherHome

    /**
     * Fonction permettant d'afficher la page pour rendre des documents
     * @param $request
     * @param $response
     * @return mixed
     */
    public function retours($request,$response){
        return $this->render($response,'Retours.html.twig');
    }//End of function retours

    /**
     * Fonction permettant de rechercher un document selon son ID ou son titre.
     * @param $request
     * @param $response
     * @param $args
     * @return false|string
     */
    public function rechercherDocument($request,$response,$args){
        $critere = $args['critere'];
        $doc = null;
        if (is_numeric($critere)){
            $doc = Document::find($critere);
        } else {
            $doc = Document::where('titre','LIKE','%'.$critere.'%')->first();
        }
        return json_encode(['document' => $doc]);
    }//End of function rechercherDocument

    /**
     * Fonction permettant de rechercher un utilisateur.
     * @param $request
     * @param $response
     * @param $args
     * @return false|string
     */
    public function rechercherUtilisateur($request,$response,$args){
        $critere = $args['critere'];
        $user = null;
        if (is_numeric($critere)){
            $user = Usager::find($critere);
        } else {
            $user = Usager::where('mail','LIKE',$critere)->first();
        }
        return json_encode(['usager' => $user]);
    }//End of function rechercherUtilisatuer

    /**
     * Fonction permettant le rendu en BDD
     * @param $request
     * @param $response
     * @return mixed
     */
    public function rendreDocuments($request,$response){
//        $val = (json_decode(file_get_contents('php://input'),true));
        $val = (!empty($_POST['docsARendre'])) ? $_POST['docsARendre'] : null;
        if (!isset($val)){
            return false;
        }
        $rendus = [];
        $val = explode(",",$val);
        foreach ($val as $value){
            $value = (int) $value;
            $doc = Document::find($value);
            $doc->etat = 0;
            $doc->save();
            $rendus[] = $doc;

            $emprunt = Emprunt::where('document_id','=',$value)
                                ->whereNull('date_retour_effective')->first();
            $emprunt->date_retour_effective = date("Y/m/d");
            $emprunt->save();
        }
        return $this->render($response,'RecapRendus.html.twig',['emprunts' => $rendus]);
    }//End of function rendreDocuments

    /**
     * méthode qui permet d'afficher la page pour ajouter un usager
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherAjoutUsager($request, $response){
        return $this->render($response, "AjoutUsager.html.twig");
    }//end of function afficherAjoutUsager

    /**
     * méthode privée qui permet de rajouter un grain de sel à des mots de passe
     * @param $password
     * @return false|string
     */
    private function saler($password)
    {
//        return password_hash("Les livres sont des amis froids et sûrs." . $password, PASSWORD_DEFAULT);
        return password_hash("Les livres sont des amis froids et sûrs." . $password, PASSWORD_DEFAULT);
//        return $password;
    }//End of function saler

    /**
     * Fonction permettant l'ajout de l'usager en BDD
     * @param $request
     * @param $response
     * @return mixed
     */
    public function ajoutUsager($request,$response){
        try{
            //on récupère les données du formulaire
            $nom = (!empty($_POST['nom'])) ? $_POST['nom'] : null;
            $prenom = (!empty($_POST['prenom'])) ? $_POST['prenom'] : null;
            $email = (!empty($_POST['email'])) ? $_POST['email'] : null;
            $adresse = (!empty($_POST['adresse'])) ? $_POST['adresse'] : null;
            $telephone = (!empty($_POST['telephone'])) ? $_POST['telephone'] : null;
            $mdp = (!empty($_POST['m2p'])) ? $_POST['m2p'] : null;

            //on verifie que les champs sont tous remplis
            if(!isset ($nom) || !isset($prenom) || ! isset($email) || !isset($adresse) || !isset($telephone) ||!isset($mdp))
                throw new \Exception("un champs requis n'a pas été rempli");

            //on filtre les données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            $adresse = filter_var($adresse, FILTER_SANITIZE_STRING);
            $telephone = filter_var($telephone, FILTER_SANITIZE_STRING);
//            $mdp = filter_var($mdp, FILTER_SANITIZE_STRING);
            $mdp_clair = $mdp;
            $mdp = $this->saler($mdp);


            //on les insère en bdd
            $usager = new Usager();
            $usager->nom = $nom;
            $usager->prenom = $prenom;
            $usager->mail = $email;
            $usager->adresse = $adresse;
            $usager->tel = $telephone;
            $usager->m2p = $mdp;
            $usager->m2p_clair = $mdp_clair;
            $usager->date_adhesion = date("Y/m/d");
            $usager->en_attente = 0;
            $usager->save();

            //envoie du mail de confirmation
            $this->envoiMail($usager->mail,
                ["nom"=>"Medianet", "prenom"=>"staff", "mail"=>"thomas.serres3@etu.univ-lorraine.fr"],
                "Acceptation de votre demande",
                "Bonjour Madame,Monsieur " . $usager->nom . ", \n" .
                "Nous avons le plaisir de vous annoncer que votre demande d'adhésion à Médianet a été acceptée. \n" .
                "Nous sommes ravis de vous compter parmi nous. \n" .
                "Nous vous souhaitons une bonne journée, \n" .
                "Cordialement, \n" .
                "L'équipe Médianet");

            //libération des variables
            unset($nom);
            unset($prenom);
            unset($email);
            unset($adresse);
            unset($telephone);
            unset($usager);

            //redirection
            $usager = Usager::all();
            return $this->redirect($response, 'voirUsagers');

        }catch (\Exception $e){
            die($e->getMessage());
        }
    }//end of function ajouterUsager

    /**
     * méthode qui permet d'afficher tous les documents 
     * @param $request
     * @param $response
     * @return mixed
     */
    public function listeDoc($request, $response, $args) {
        $doc = Document::all();
        return $this->render($response, "ListeDoc.html.twig", ['document'=>$doc]);
    }//End of function listeDoc

    /**
     * méthode qui permet d'afficher le formulaire pour modifier un document
     * @param $request
     * @param $response
     * @return mixed
     */
    public function formDocModif($request, $response, $args) {
        $doc = Document::find(intVal($args['id']));
        return $this->render($response, 'FormDocModif.html.twig', ['document'=>$doc]);
    }//End of function formDocModif

    /**
     * méthode qui permet de modifier le document (ajout dans le bdd)
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     * @throws \Exception
     */
    public  function modifDoc($request, $response, $args)
    {

        $cheminImg = $GLOBALS['PUBLIC'] . "images/";
        
        $doc = Document::find(intVal($args['id']));
        $doc->etat = $_POST['etat'];
        $doc->genre = $_POST["genre"];
        $doc->titre = $_POST["titre"];
        $doc->auteur = $_POST["auteur"];
        $doc->description = $_POST["description"];
        $doc->mots_cles = $_POST["mc"];
        $doc->duree = $_POST["duree"];
        $doc->acteurs = $_POST["acteurs"];
        $doc->nbPages = $_POST["nbPages"];
        $doc->editeur = $_POST["editeur"];
        if ($_POST["illustration"] == ""){
            $doc->illustration = "https://www.vermeer.com.au/wp-content/uploads/2016/12/attachment-no-image-available.png";
          }
          else {
            $doc->illustration = $_POST["illustration"];
          }

        $doc->save();
        $doc = Document::all();
        return $this->redirect($response, 'listeDoc');

    }//End of function modifDoc

    /**
     * méthode qui permet de supprimer un document 
     * @param $request
     * @param $response
     * @return mixed
     */
    public function supprimerDoc($request, $response, $args){
        //  $doc = Document::find($_POST["id"]);
          $doc = Document::find(intVal($args['id']));
          $doc->delete();
          $doc = Document::all();
          return $this->redirect($response, 'listeDoc');
        }//End of function supprimerDoc

    /**
     * méthode voirUsagers qui permet l'affichage de la vue de la liste des usagers
     * @param $request
     * @param $response
     */
    public function voirUsagers($request, $response){
        $usagers = Usager::where("en_attente", "=", "0")->get();
        $this->render($response, "ListeUtilisateur.html.twig", ["usagers" => $usagers]);
    }//end of function voirUsagers

    /**
     * méthode qui permet de supprimer l'usager dont l'id est passé en paramètre de l'url
     * @param $request
     * @param $response
     * @param $args
     * @return bool
     */
    public function supprimerUsager($request, $response, $args){
        $user = Usager::find(intVal($args['id']));
        $user->delete();
        $user = Usager::all();
        return $this->redirect($response, 'voirUsagers');  
    }//end of function supprimerUsagers

    /**
     * méthode qui permet d'afficher la vue pour la modification d'un usager
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function afficherModifierUsager($request, $response, $args){
        try{
            $id = (isset($args['id'])) ? $args['id'] : null;
            if(!isset($id)) throw new \Exception("aucun id rentré");

            $usager = Usager::find($id);
            if(!isset($usager))
                throw new \Exception("L'usager n'existe pas");

            return $this->render($response, "ModifUsager.html.twig", ["usager" => $usager]);
        }catch(\Exception $e){
            die($e->getMessage());
        }
    }//end of function afficherModifierUsager

    /**
     * Méthode post, modifiant l'usager dans la BDD
     * @param $request
     * @param $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function modifierUsager($request, $response){
        try{
            //Récupération des données du POST
            $id = (isset($_POST['id'])) ? $_POST['id'] : null;
                if(!isset($id)) throw new \Exception("aucun utilisateur à modifier"); //on vérifie qu'on a bien un id avant de commencer
            $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
            //on regarde si un usager existe bien pour cet id
            $usager = Usager::find($id);
            if(!isset($usager) || empty($usager)) throw new \Exception("Aucun usager n'existe pour cet id");

            //suite récupération des données du post
            $nom = (isset($_POST['nom'])) ? $_POST['nom'] : null;
            $prenom = (isset($_POST['prenom'])) ? $_POST['prenom'] : null;
            $email = (isset($_POST['email'])) ? $_POST['email'] : null;
            $adresse = (isset($_POST['adresse'])) ? $_POST['adresse'] : null;
            $telephone = (isset($_POST['telephone'])) ? $_POST['telephone'] : null;

            //on verifie que les champs sont tous remplis
            if(!isset ($nom) || !isset($prenom) || ! isset($email) || !isset($adresse) || !isset($telephone))
                throw new \Exception("un champs requis n'a pas été rempli");

            //on filtre les données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            $adresse = filter_var($adresse, FILTER_SANITIZE_STRING);
            $telephone = filter_var($telephone, FILTER_SANITIZE_STRING);

            //on enregistre les modifications
            $usager->nom = $nom;
            $usager->prenom = $prenom;
            $usager->mail = $email;
            $usager->adresse = $adresse;
            $usager->tel = $telephone;
            $usager->save();

            //redirection
            return $this->redirect($response, "voirUsagers");

        }catch(\Execption $e){
            die($e->getMessage());
        }
    }//end of function modifierUsager

    /**
     * Fonction affichant la page d'emprunts.
     * @param $request
     * @param $response
     * @return mixed
     */
    public function emprunts($request,$response){
        return $this->render($response,'Emprunts.html.twig');
    }

    /**
     * Fonction permettant d'emprunter les documents, les id des documents et de l'utilisateur sont envoyés en POST, et traité ici.
     * @param $request
     * @param $response
     * @throws \Exception
     */
    public function emprunterDocuments($request,$response){
        $valeurs = (json_decode(file_get_contents('php://input'),true));
        $valDocs = $valeurs[0];
        $valUser = $valeurs[1];

//        Pour chaque ID de document dans le tableau.
        foreach ($valDocs as $value){
            $doc = Document::find($value);
            $doc->etat = 1;
            $doc->save();
            unset($doc);

            $emprunt = new Emprunt();
            $emprunt->usager_id = $valUser;
            $emprunt->document_id = $value;
            $emprunt->date_emprunt = date("Y/m/d");
            //Création de la date de retour (deux semaines après.)
            $dateTime = new \DateTime(date("Y/m/d"));
            $dateTime->modify('+2 weeks');
            $tabdate = explode(' ',$dateTime->format('Y-m-d'));
            $emprunt->date_retour_prevue = $tabdate[0];
            $emprunt->save();
            unset($emprunt);
            unset($dateTime);
            unset($tabdate);

        }

    }//End of function emprunterDocuments

    /**
     * Fonction permettant l'affichage de la page de récapitulatifs des emprunts, de l'utilisateur
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function recapEmprunts($request,$response,$args){
        $id = (int) $args['id'];
        $emprunts = DB::table('DOCUMENT')
            ->join('EMPRUNT','DOCUMENT.document_id','=','EMPRUNT.document_id')
            ->join('USAGER','USAGER.usager_id','=','EMPRUNT.usager_id')
            ->where('DOCUMENT.etat','=',1)
            ->where('USAGER.usager_id','=',$id)
            ->whereNull('date_retour_effective')->get();
        return $this->render($response, 'RecapEmprunts.html.twig',['emprunts' => $emprunts]);
    }//End of function recapEmprunts

    /**
     * Fonction permettant d'afficher la page d'accueil.
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherAccueil($request,$response){
        return $this->render($response,'Accueil.html.twig');
    }//End of function afficherAccueil

    /**
     * méthode qui permet l'affichade des demandes d'adhésion à statuer
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherGestionAdhesion($request, $response){
        $documents = Usager::where("en_attente", "=", 1)->get();
        return $this->render($response, "GererAdhesions.html.twig", ["usagers" => $documents]);
    }

    /**
     *méthode qui permet d'accepter une demande d'adhésion
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function accepterAdhesion($request, $response, $args){
        try{
            $id = $args['id'];
            if(!isset($id)) throw new \Exception("aucun id n'est passé en paramètre");

            //récupération de l'usager
            $usager = Usager::find($id);
            if(!isset($usager)) throw new \Exception("Aucun usager ne correspond à cet id");

            $usager->en_attente = 0;
            $usager->save();


            //envoie du mail

            $this->envoiMail($usager->mail,
                 ["nom"=>"Medianet", "prenom"=>"staff", "mail"=>"thomas.serres3@etu.univ-lorraine.fr"],
                "Acceptation de votre demande",
                "Bonjour Madame,Monsieur " . $usager->nom . ", \n" .
                "Nous avons le plaisir de vous annoncer que votre demande d'adhésion à Médianet a été acceptée. \n" .
                "Nous sommes ravis de vous compter parmi nous. \n" .
                "Nous vous souhaitons une bonne journée, \n" .
                "Cordialement, \n" .
                "L'équipe Médianet");

            unset($usager);// libération de la variable $usager

            //on redirige vers la page de gestion des demandes
            return $this->redirect($response, "gererDemandesAdhesion");
        }catch(\Exception $e){
            die($e->getMessage());
        }
    }


    /**
     *méthode qui permet de refuser une demande d'adhésion
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function refuserAdhesion($request, $response, $args){
        try{
            $id = $args['id'];
            if(!isset($id)) throw new \Exception("aucun id n'est passé en paramètre");

            //récupération de l'usager
            $usager = Usager::find($id);
            if(!isset($usager)) throw new \Exception("Aucun usager ne correspond à cet id");

            $usager->en_attente = 2;
            $usager->save();

            //envoie du mail

            $this->envoiMail($usager->mail,
                 ["nom"=>"Medianet", "prenom"=>"staff", "mail"=>"thomas.serres3@etu.univ-lorraine.fr"],
                "refus de votre demande",
                "Bonjour Madame,Monsieur " . $usager->nom . ", \n" .
                "Nous sommes au regret de vous annoncer que votre demande d'adhésion à Médianet a été refusée. \n" .
                "Nous vous souhaitons une bonne journée, \n" .
                "Cordialement, \n" .
                "L'équipe Médianet");

            unset($usager);// libération de la variable $usager

            //on redirige vers la page de gestion des demandes
            return $this->redirect($response, "gererDemandesAdhesion");
        }catch(\Exception $e){
            die($e->getMessage());
        }
    }

    /**
     * methode privee qui sert à envoyer des mails
     * prends en parametre l'adresse email du destinataire, un tableau avec le nom, le prénom et l'email de l'envoyeur,
     * le sujet du message et le message
     * @param $to
     * @param array $from
     * @param $subject
     * @param $message
     * @return bool
     */
    private function envoiMail($to, $from = array("nom" => "", "prenom" => "", "mail" => ""), $subject, $message){
        $nom = $from['nom'];
        $prenom = $from['prenom'];
        $mail = $from['mail'];

        $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);

        $headers = "From: $prenom $nom <$mail>\r\nReply-To: $mail";
        $headers .= "Content-type: text/html; charset=\"utf-8\"\r\n";
        $retour = false;

        try{
            mail($to, $subject, $message, $headers);
            $retour = true;
        }catch(\Exception $e){
            $retour = false;
        }

        return $retour;
    }

    /**
     * méthode qui permet d'envoyer des mails de rappel en fonction d'un id d'emprunt
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function envoieMailRappel($request, $response, $args){
        try {
            $id = $args['emprunt_id'];
            if (!isset($id)) throw new \Exception("aucun id n'est passé en paramètre");

            //récupération de l'emprunt
            $emprunt = Emprunt::find($id);
            if(!isset($emprunt)) throw new \Exception("aucun emprunt ne correspond à cet id");

            //récupération du document
            $doc = Document::find($emprunt->document_id);
            if(!isset($emprunt)) throw new \Exception("aucun document ne correspond à cet id");

            //récupération de l'usager
            $usager = Usager::find($emprunt->usager_id);
            if (!isset($usager)) throw new \Exception("Aucun usager ne correspond à cet id");

            //création du message
            $message = "Bonjour Madame,Monsieur " . $usager->nom . ", \n" .
                "nous vous informons que vous devez rapporter le document " . $doc->titre . ", emprunté le " . $this->dateEnFr($emprunt->date_emprunt) . ". \n"
                . "Bonne journée, \n Cordialement, \n L'équipe Médianet";

            //envoie du mail
            $this->envoiMail($usager->mail,   ["nom"=>"Medianet", "prenom"=>"staff", "mail"=>"thomas.serres3@etu.univ-lorraine.fr"], "Médianet : Retour de document", $message);

            //redirection
            return $this->redirect($response, "dashBoard");
        }
        catch(\Exception $e){
            die($e->getMessage());
        }
    }

    /**
     * methode privee qui fait la conversion d'une date en en date fr
     * @param $date
     * @return string
     */
    private function dateEnFr($date){
        $tmp = explode("-", $date);
        return "" . $tmp[2] . "/" . $tmp[1] . "/" . $tmp[0];
    }

    /**
     * méthode qui permet de réinitialiser un mot de passe utilisateur
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function reinitialiserMdp($request, $response, $args){
        try {
            $id = $args['id'];
            if (!isset($id)) throw new \Exception("aucun id n'est passé en paramètre");

            //récupération de l'usager
            $usager = Usager::find($id);
            if (!isset($usager)) throw new \Exception("Aucun usager ne correspond à cet id");

            //changement du mot de passe en bdd
            $usager->m2p = $this->saler("password");
            $usager->m2p_status = 1;
            $usager->save();

            //création du message
            $message = "Bonjour Madame,Monsieur " . $usager->nom . ", \n" .
                "nous avons réinitialisé votre mot de passe. Un mot de passe provisoire vous est donné. Il s'agit de password ; lors de votre prochaine connexion merci de le changer. \n"
                . "Bonne journée, \n Cordialement, \n L'équipe Médianet";

            //envoie du mail
            $this->envoiMail($usager->mail,   ["nom"=>"Medianet", "prenom"=>"staff", "mail"=>"thomas.serres3@etu.univ-lorraine.fr"], "Médianet : Réinitialisation de votre mot de passe", $message);

            //redirection
            return $this->redirect($response, "voirUsagers");
        }
        catch(\Exception $e){
            die($e->getMessage());
        }
    }
}
