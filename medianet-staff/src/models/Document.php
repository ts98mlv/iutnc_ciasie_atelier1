<?php


namespace medianet_staff\models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'DOCUMENT';
    protected $primaryKey = 'document_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}