<?php


namespace medianet_staff\models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Emprunt extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'EMPRUNT';
    protected $primaryKey = 'emprunt_id';
    public $timestamps = false;

    protected $fillable = ['usager_id', 'document_id'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];


}