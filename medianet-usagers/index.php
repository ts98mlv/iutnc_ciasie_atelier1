<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use bonduelle\models\Personnage;

require __DIR__ . '/src/config/config.inc.php';
require __DIR__ . '/vendor/autoload.php';

// Create container
$container = array();


///////////
// TWIG //
//////////

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('src/views', ["debug" => true]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    $view->addExtension(new \Twig_Extension_Debug());

    return $view;
};


///////////////
// ELOQUENT //
//////////////
$container['settings'] = $config;
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

// Init Slim
$app = new \Slim\App($container);

//session_start
session_start();


//////////////
// ROUTAGE //
/////////////
$app->get('/nom', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hello");
    return $response;
}); 
$app->get('/', "\\medianet_usagers\\controllers\\UsagersController:Index")->setName("index");
$app->get('/index', "\\medianet_usagers\\controllers\\UsagersController:Index")->setName("index");
$app->post('/connecte',"\\medianet_usagers\\controllers\\AuthentificationController:authentifier");
$app->get('/profil', "\\medianet_usagers\\controllers\\UsagersController:indexProfil")->setName("profil")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->post('/profil', "\\medianet_usagers\\controllers\\AuthentificationController:modifUsager")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->get('/documents', "\\medianet_usagers\\controllers\\DocumentsController:Index")->setName("documents")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->get('/livres', "\\medianet_usagers\\controllers\\LivresController:Index")->setName("livres")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->get('/cds', "\\medianet_usagers\\controllers\\CdsController:Index")->setName("cds")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->get('/dvds', "\\medianet_usagers\\controllers\\DvdsController:Index")->setName("dvds")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->get('/inscription', "\\medianet_usagers\\controllers\\InscriptionController:Index")->setName("inscription");
$app->post('/inscription', "\\medianet_usagers\\controllers\\InscriptionController:inscriptionUsager");

$app->get("/emprunts", "\\medianet_usagers\\controllers\\DocumentsController:afficherDocumentsEmpruntes")->setName("documentsEmpruntes")->add(new \medianet_usagers\middlewares\SessionMiddleware());
$app->post("/recherche", "\\medianet_usagers\\controllers\\UsagersController:recherche")->add(new \medianet_usagers\middlewares\SessionMiddleware());


/////////////
// RUN     //
/////////////
$app->run();
