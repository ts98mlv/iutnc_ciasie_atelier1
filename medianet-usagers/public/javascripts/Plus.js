let buttons = document.querySelectorAll(".btn_sombre");
for (i = 0; i < buttons.length; i++){
    element = buttons[i];
    element.addEventListener("click",function(event){

        //On affiche le div 
        document.getElementById("plus").style.display = "block"

        //On remplit le div
        if (this.dataset.type == 0){
            document.getElementById("D_titre").innerHTML = "<i class='fas fa-book'></i>" +" "+ this.dataset.titre
        }   
        else if (this.dataset.type == 1){
            document.getElementById("D_titre").innerHTML = "<i class='fas fa-video'></i>" +" "+  this.dataset.titre
        } 
        else{
            document.getElementById("D_titre").innerHTML = "<i class='fas fa-compact-disc'></i>" +" "+  this.dataset.titre
        }    
        
        document.getElementById("D_img").src = this.dataset.img
        document.getElementById("D_img").width = 350
        document.getElementById("D_descr").innerHTML = this.dataset.descr
        document.getElementById("D_etat").innerHTML = "<b>Etat: </b> " + this.dataset.etat
        document.getElementById("D_genre").innerHTML = "<b>Genre: </b> " + this.dataset.genre 
        document.getElementById("D_id").innerHTML = "<b>Id: </b> " + this.dataset.idd
        document.getElementById("D_mot").innerHTML = "<b>Mots-clés: </b> " + this.dataset.mot


        if (this.dataset.duree){
            document.getElementById("D_duree").innerHTML = "<b>Durée: </b> " + this.dataset.duree
        } 
        else document.getElementById("D_duree").innerHTML = ""
        if (this.dataset.acteurs){
            document.getElementById("D_acteurs").innerHTML = "<b>Acteurs: </b> " + this.dataset.acteurs
        }
        else document.getElementById("D_acteurs").innerHTML = ""
        if (this.dataset.nbpages){
            document.getElementById("D_nbPages").innerHTML = "<b>Nombre de pages: </b> " + this.dataset.nbpages
        }
        else document.getElementById("D_nbPages").innerHTML = ""
        if (this.dataset.editeur){
            document.getElementById("D_editeur").innerHTML = "<b>Editeur: </b> " + this.dataset.editeur
        }
        else document.getElementById("D_editeur").innerHTML = ""
    })
}
//On cache les infos supplémentaires par défaut

document.getElementById("plus").style.display = "none"

function descendre(){
    $("html, body").animate({ scrollTop: $(document).height() }, 1000); 
}

function remonter(){
    $("html, body").animate({ scrollTop: 0}, 1000);
}