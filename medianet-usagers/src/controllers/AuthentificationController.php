<?php

namespace medianet_usagers\controllers;

use medianet_usagers\models\Document;
use medianet_usagers\models\Usager;
use medianet_usagers\models\Emprunt;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class AuthentificationController extends BaseController{
   
    /**
     * méthode qui permet de vérifier la conformité de deux mots de passe
     * @param $password_entry
     * @param $paswword_bdd
     * @return bool
     */
    private function verifyPassword($password_entry, $paswword_bdd)
    {     
            return password_verify("Les livres sont des amis froids et sûrs.". $password_entry, $paswword_bdd);
    }

    /**
     * méthode qui permet de saler
     * @param $password
     * @return mixed
     */
    private function saler($password)
    {
        return password_hash("Les livres sont des amis froids et sûrs." . $password, PASSWORD_DEFAULT);
    }



    /**
     * méthode qui permet de s'identifier
     * @param $request
     * @param $response
     * @return mixed
     */
    public function authentifier($request,ResponseInterface $response){
            try{
                if (isset($_SESSION["erreur"])){
                    unset($_SESSION["erreur"]);
                }
                //Récupérer données du formulaire
                $id = (isset($_POST["idconnexion"])) ? $_POST["idconnexion"] : null;
                $mdp = (isset($_POST["mdp"])) ? $_POST["mdp"] : null;
                
                //Tester les données
                if(!isset($id) || !isset($mdp))
                    throw new \Exception ("L'adresse mail ou le mot de passe sont incorrects !");
                
                //Filtrer les données
                $id = filter_var($id,FILTER_SANITIZE_EMAIL);
                

                //Eloquent <3
                $testid = Usager::where('mail','=',$id)->first();
                if (!isset($testid)){
                    throw new \Exception ("L'adresse mail est incorrecte !");
                }

                if ($this->verifyPassword($mdp,$testid->m2p)){
                    //Si l'utilisateur n'est pas en attente
                    if ($testid->en_attente !== 1){
                       //Stockage en session
                        $_SESSION["user_id"] = $testid->usager_id;

                         //On redirige
                        //si le mot de passe est provisoire on redirige vers la page profil pour le modifier
                        if($testid->m2p_status == 1)
                            return $this->redirect($response, "profil");
                        else
                            return $this->redirect($response,'documentsEmpruntes');
                    }
                    else throw new \Exception ("Votre compte n'est pas encore activé !");//sinon

                    
                }
                else{
                    throw new \Exception ("Le mot de passe est incorrect !");
                }
            }
            catch(\Exception $e){
              
               $_SESSION["erreur"] = $e->getMessage();
               return $this->redirect($response,'index');
            }
    }
    

    /**
     * méthode qui permet de modifier un usager
     * @return mixed
     */
    public function modifUsager($request, $response){
        try{
            //on récupère les données du formulaire
            $nom = (isset($_POST['nom'])) ? $_POST['nom'] : null;
            $prenom = (isset($_POST['prenom'])) ? $_POST['prenom'] : null;
            $email = (isset($_POST['email'])) ? $_POST['email'] : null;
            $adresse = (isset($_POST['adresse'])) ? $_POST['adresse'] : null;
            $telephone = (isset($_POST['telephone'])) ? $_POST['telephone'] : null;
            $mdp = (isset($_POST['m2p'])) ? $_POST['m2p'] : null;

            //on verifie que les champs sont tous remplis
            if(!isset ($nom) || !isset($prenom) || ! isset($email) || !isset($adresse) || !isset($telephone) ||!isset($mdp))
                throw new \Exception("un champs requis n'a pas été rempli");

            //on filtre les données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            $adresse = filter_var($adresse, FILTER_SANITIZE_STRING);
            $telephone = filter_var($telephone, FILTER_SANITIZE_STRING);
            $mdp = $this->saler($mdp);


            //on les insère en bdd
            $usager = Usager::find($_SESSION["user_id"]);
            $usager->nom = $nom;
            $usager->prenom = $prenom;
            $usager->mail = $email;
            $usager->adresse = $adresse;
            $usager->tel = $telephone;
            $usager->m2p = $mdp;
            $usager->m2p_status = 0;
            $usager->save();

            //libération des variables
            unset($nom);
            unset($prenom);
            unset($email);
            unset($adresse);
            unset($telephone);
            unset($usager);
            unset($mdp);
            //redirection
            return $this->redirect($response, "documentsEmpruntes");
        }catch (\Exception $e){
            die($e->getMessage());
        }
    }//end of function ModifierUsager 
    
}

   

