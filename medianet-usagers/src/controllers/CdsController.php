<?php

namespace medianet_usagers\controllers;

use medianet_usagers\models\Document;
use medianet_usagers\models\Usager;
use medianet_usagers\models\Emprunt;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class CdsController extends BaseController{

    /**
     * méthode qui permet de voir les cds
     * @param $request
     * @param $response
     * @return mixed
     */
    public function Index($request,$response){
        $documents = Document::all();
        return $this->render($response,'Cds.html.twig',["Documents" => $documents]);
    }
        
}

   
