<?php

namespace medianet_usagers\controllers;

use medianet_usagers\models\Document;
use medianet_usagers\models\Usager;
use medianet_usagers\models\Emprunt;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class DocumentsController extends BaseController{

    /**
     * méthode afficherDocuments qui permet d'afficher la liste des documents 
     * @param $request
     * @param $response
     * @return mixed
     */
    public function Index($request,$response){
        $documents = Document::all();
        return $this->render($response,'Documents.html.twig',["Documents" => $documents]);
    }

    /**
     * méthode afficherDocumentsEmpruntes qui permet d'afficher la liste des documents empruntés
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherDocumentsEmpruntes($request, $response){
        $documents_empruntes = DB::table("DOCUMENT")
                                ->join("EMPRUNT", "DOCUMENT.document_id", "=", "EMPRUNT.document_id")
                                ->where("DOCUMENT.etat", "=", 1)
                                ->whereNull("date_retour_effective")
                                ->where("EMPRUNT.usager_id", "=", $_SESSION['user_id'])
                                ->get();

        return $this->render($response, "Emprunts.html.twig", ["documents" => $documents_empruntes]);
    }//end of function afficherDocumentsEmpruntes
}

   

