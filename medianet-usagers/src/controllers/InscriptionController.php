<?php

namespace medianet_usagers\controllers;

use medianet_usagers\models\Document;
use medianet_usagers\models\Usager;
use medianet_usagers\models\Emprunt;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class InscriptionController extends BaseController{

    /**
     * méthode qui permet de d'aller sur la page pour s'inscrire
     * @param $request
     * @param $response
     * @return mixed
     */
    public function Index($request,$response){
        return $this->render($response,'Inscription.html.twig');
    }
    /**
     * méthode qui permet de saler
     * @param $password
     * @return mixed
     */
    private function saler($password)
    {
        return password_hash("Les livres sont des amis froids et sûrs." . $password, PASSWORD_DEFAULT);
    }

    /**
     * méthode qui permet de demander à s'inscrire
     * @return mixed
     */
    public function inscriptionUsager($request,$response){
        try{
            //on récupère les données du formulaire
            $nom = (isset($_POST['nom'])) ? $_POST['nom'] : null;
            $prenom = (isset($_POST['prenom'])) ? $_POST['prenom'] : null;
            $email = (isset($_POST['email'])) ? $_POST['email'] : null;
            $adresse = (isset($_POST['adresse'])) ? $_POST['adresse'] : null;
            $telephone = (isset($_POST['telephone'])) ? $_POST['telephone'] : null;
            $mdp = (isset($_POST['m2p'])) ? $_POST['m2p'] : null;
            $mdpconf = (isset($_POST['m2pconf'])) ? $_POST['m2pconf'] : null;

            //on verifie que les champs sont tous remplis
            if(!isset ($nom) || !isset($prenom) || ! isset($email) || !isset($adresse) || !isset($telephone) ||!isset($mdp) ||!isset($mdpconf))
                throw new \Exception("Tous les champs doivent être remplis");

            //on verifie que les deux mdp sont identiques
            if ($mdp != $mdpconf){
                throw new \Exception("Les mots de passe doivent être identiques");
            }

            //on filtre les données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            $adresse = filter_var($adresse, FILTER_SANITIZE_STRING);
            $telephone = filter_var($telephone, FILTER_SANITIZE_STRING);
            $mdp = $this->saler($mdp);


            //on les insère en bdd
            $usager = new Usager;
            $usager->nom = $nom;
            $usager->prenom = $prenom;
            $usager->mail = $email;
            $usager->adresse = $adresse;
            $usager->tel = $telephone;
            $usager->m2p = $mdp;
            $usager->date_adhesion = Date("Y-m-d");
            $usager->save();

            //libération des variables
            unset($nom);
            unset($prenom);
            unset($email);
            unset($adresse);
            unset($telephone);
            unset($usager);
            unset($mdp);
            unset($mdpconf);
            

            //redirection
            return $this->redirect($response,'index');

        }catch (\Exception $e){
            
            die($e->getMessage());
        }
    }//end of function inscriptionUsager
    
    }
