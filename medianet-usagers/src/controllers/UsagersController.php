<?php

namespace medianet_usagers\controllers;

use medianet_usagers\models\Document;
use medianet_usagers\models\Usager;
use medianet_usagers\models\Emprunt;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class UsagersController extends BaseController{
    /**
     * méthode qui permet de voir les usagers
     * @param $request
     * @param $response
     * @return mixed
     */
    public function Index($request,$response){
        if (isset($_SESSION["erreur"])){
            echo "<p style='color:  #f0bcb1  ;text-align:center;'>",$_SESSION["erreur"],"</p>";
        }
        if (isset($_SESSION["user_id"])){
            unset($_SESSION["user_id"]);
        }
        return $this->render($response,'Accueil.html.twig');
    }
    /**
     * méthode qui permet de voir son profil avec eloquent
     * @param $request
     * @param $response
     * @return mixed
     */
    public function indexProfil($request,$response){
        $id = $_SESSION["user_id"];
        
        $usager = Usager::find($id);
        return $this->render($response,'Profil.html.twig',["Usager" => $usager]);
    }

    /**
     * fonction qui permet de rechercher et afficher les résultats de la recherche
     * @param $request
     * @param $response
     * @return mixed
     */
    public function recherche($request, $response){
            $res = null;

        try{
            //récupération du champs de recherche
            $recherche = (isset($_POST["search"])) ? $_POST["search"] : null;

            //verification que le champs n'est pas nul
            if(!isset($recherche)) throw new \Exception("la recherche est vide");

            //on regarde si la recherche est un id
            if(is_numeric($recherche)){
                $res = Document::where("document_id", "=", $recherche)->get();
            }//sinon on recherche dans les titres, les descriptions et les mots-cles
            else{
                $res = Document::where("titre", "LIKE", "%" . $recherche . "%")
                        ->orWhere("description", "LIKE", "%" . $recherche . "%")
                        ->orWhere("mots_cles", "LIKE", "%" . $recherche . "%")
                        ->get();
            }

            //affiche la vue avec les résultats de la recherche
            return $this->render($response, "Recherche.html.twig", ["recherche" => $recherche,
                "documents" => $res]);

        }catch(\Exception $e){
            die($e->getMessage());
        }

    }

    
}

   

