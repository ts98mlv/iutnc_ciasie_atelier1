<?php

namespace medianet_usagers\middlewares;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class SessionMiddleware {

    /**
     * méthode invoquée lors de l'utilisation du middleware
     * @param $request
     * @param $response
     * @param $next
     * @return ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {

        //si on est pas connecté on redirige sur la page de connexion
        if (!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {

            return $response->withStatus(302)->withHeader('Location', 'index');
        }
        $response = $next($request, $response);

        return $response;
    }
}