<?php


namespace medianet_usagers\models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Usager extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'USAGER';
    protected $primaryKey = 'usager_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}