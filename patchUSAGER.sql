USE medianet;
ALTER TABLE `USAGER` ADD `en_attente` INTEGER NULL DEFAULT '1' AFTER `deleted_at`;
ALTER TABLE `USAGER` ADD `m2p_status` INT NOT NULL DEFAULT '0' AFTER `en_attente`;
